/*
 * Copyright (C) 2019  Ortega Froysa, Nicolás <nortega@themusicinnoise.net>
 * Author: Ortega Froysa, Nicolás <nortega@themusicinnoise.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <signal.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <X11/Xlib.h>

Display *display;
Window clip_win;
int run;
void *data;
size_t data_size;

void quit() {
	run = 0;
	XClientMessageEvent cme;
	cme.type = ClientMessage;
	cme.format = 32;
	XSendEvent(display, clip_win, 0, 0, (XEvent*)&cme);
	XFlush(display);
}

void get_selection(Atom property) {
	Atom da, incr, type;
	int di;
	unsigned long size, dul;
	unsigned char *prop_ret = NULL;

	XGetWindowProperty(display, clip_win, property, 0, 0, 0, AnyPropertyType,
			&type, &di, &dul, &size, &prop_ret);
	XFree(prop_ret);

	incr = XInternAtom(display, "INCR", 0);
	if(type == incr)
	{
		// TODO: prepare for multiple messages
		return;
	}

#ifdef DEBUG
	printf("Property size: %lu\n", size);
#endif
	XGetWindowProperty(display, clip_win, property, 0, size, 0, AnyPropertyType,
			&da, &di, &dul, &dul, &prop_ret);
	free(data);
	data = malloc(size);
	strcpy(data, (void*)prop_ret);
	data_size = size;
#ifdef DEBUG
	printf("Received data: %s\n", (char*)data);
#endif
	XFree(prop_ret);
	XDeleteProperty(display, clip_win, property);
}

void send_reject(XSelectionRequestEvent *sre) {
	XSelectionEvent reply_se;

#ifdef DEBUG
	char *target_name;
	target_name = XGetAtomName(display, sre->target);
	printf("Denying request for type `%s'\n", target_name);
	if(target_name)
		XFree(target_name);
#endif

	reply_se.type = SelectionNotify;
	reply_se.requestor = sre->requestor;
	reply_se.selection = sre->selection;
	reply_se.target = sre->target;
	reply_se.property = None;
	reply_se.time = sre->time;

	XSendEvent(display, sre->requestor, 1,
			NoEventMask, (XEvent*)&reply_se);
}

void send_selection(XSelectionRequestEvent *sre, Atom target)
{
	XSelectionEvent reply_se;

#ifdef DEBUG
	char *target_name = XGetAtomName(display, sre->property);
	printf("Sending CLIPBOARD to window 0x%1x, property '%s'\n",
			(unsigned int)sre->requestor, target_name);
	if(target_name)
		XFree(target_name);
#endif

	XChangeProperty(display, sre->requestor, sre->property,
			target, 8, PropModeReplace, (unsigned char*)data, data_size);

	reply_se.type = SelectionNotify;
	reply_se.requestor = sre->requestor;
	reply_se.selection = sre->selection;
	reply_se.target = sre->target;
	reply_se.property = sre->property;
	reply_se.time = sre->time;

	XSendEvent(display, sre->requestor, 1,
			NoEventMask, (XEvent*)&reply_se);
}

int main() {
	Window root, owner;
	int screen;
	Atom selection, clip_property;
	// target atoms
	Atom utf8_target;//, text_target, string_target;
	data = NULL;
	data_size = 0;

	// get current X display
	display = XOpenDisplay(NULL);
	if(!display)
	{
		fprintf(stderr, "Could not open X display.\n");
		return 1;
	}
	screen = DefaultScreen(display);
	root = RootWindow(display, screen);

	// create a dummy window for tinyclip
	clip_win = XCreateSimpleWindow(display, root, -10, -10, 1, 1, 0, 0, 0);
	XSelectInput(display, clip_win,
			SelectionClear | SelectionRequest | SelectionNotify | ClientMessage);

	// create atoms for selections
	selection = XInternAtom(display, "CLIPBOARD", 0);
	utf8_target = XInternAtom(display, "UTF8_STRING", 0);
	//text_target = XInternAtom(display, "TEXT", 0);
	//string_target = XInternAtom(display, "STRING", 0);
	clip_property = XInternAtom(display, "TINYCLIP", 0);

	// check if there is currently someone using the clipboard
	owner = XGetSelectionOwner(display, selection);
	if(owner != None)
	{
		// take ownership of clipboard while copying contents
#ifdef DEBUG
		printf("Current owner: 0x%1X\n", (unsigned int)owner);
#endif
		XConvertSelection(display, selection, utf8_target,
				clip_property, clip_win, CurrentTime);
	}
	else
	{
		XSetSelectionOwner(display, selection, clip_win, CurrentTime);
	}

	run = 1;
	signal(SIGINT, quit);

	Atom wmDeleteMessage = XInternAtom(display, "WM_DELETE_WINDOW", 0);
	XSetWMProtocols(display, clip_win, &wmDeleteMessage, 1);

	while(run)
	{
		XEvent event;
		XSelectionRequestEvent *sre;
		XSelectionEvent *se;
		XNextEvent(display, &event);
		switch(event.type)
		{
			case SelectionClear:
				// if someone else took the clipboard, regain ownership
				XConvertSelection(display, selection, utf8_target,
						clip_property, clip_win, CurrentTime);
				break;
			case SelectionNotify:
				// we've been sent the new CLIPBOARD, now we take ownership of it
				se = (XSelectionEvent*)&event.xselection;
				if(se->property == None)
					fprintf(stderr, "Conversion impossible.");
				else
					get_selection(clip_property);
				XSetSelectionOwner(display, selection, clip_win, CurrentTime);
				break;
			case SelectionRequest:
				// respond to request for clipboard
				sre = (XSelectionRequestEvent*) &event.xselectionrequest;
#ifdef DEBUG
				printf("New selection request:\n"
						"  requestor: 0x%1x\n", (unsigned int)sre->requestor);
#endif
				if(sre->target != utf8_target || sre->property == None)
					send_reject(sre);
				else
					send_selection(sre, utf8_target);
				break;
			case ClientMessage:
				break;
		}
	}

	return 0;
}
